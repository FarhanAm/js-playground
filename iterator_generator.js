//------------ Iterator
function rangeIteratorFunction (start = 0, end = Infinity, step = 1) {
    let nextStep = start;

    const rangeIterator = {
        next: function () {
            let result;
            if (nextStep < end) {
                result = { value: nextStep, done: false };
                nextStep += step;
                return result;
            }
            return { value: (nextStep - start) / step, done: true };
        }
    };
    return rangeIterator;
}

const iterator1 = rangeIteratorFunction(10, 20, 2);
let result = iterator1.next();
while (result.done == false) {
    console.log(result.value);
    result = iterator1.next();
}
console.log(result.value);

console.log("iterator 2:");

const iterator2 = rangeIteratorFunction(0, 10, 3);
let result2 = iterator2.next();
while (result2.done == false) {
    console.log(result2.value);
    result2 = iterator2.next();
}
console.log(result2.value);

//-----------generator
function* generateSequence () {
    yield 1;
    yield 2;
    return 3;
};

let generator1 = generateSequence();

let one = generator1.next();
console.log(JSON.stringify(one));
let two = generator1.next();
console.log(JSON.stringify(two));
let three = generator1.next();
console.log(JSON.stringify(three));

// generators are iterable
let generator2 = generateSequence();
for (let val of generator2) {
    console.log(val); // this wont reach 3, must return values with yeild
}

let generator3 = generateSequence();
let myArr = [0, ...generator3];
console.log(myArr);

// iterable generator
function* rangeGeneratorFunc (start = 0, end = 10, step = 1) {
    for (let i = start; i < end; i += step) {
        yield i;
    }
}

const generator4 = rangeGeneratorFunc(1, 10, 2);
for (const item of generator4) {
    console.log(item);
}

console.log(generator4[Symbol.iterator]() == generator4);

const generator5 = rangeGeneratorFunc(2, 7, 1);
const myArr2 = [0, 1, ...generator5];
console.log("Array with generator:", myArr2);
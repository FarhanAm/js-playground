//---------- Objects
const myCar = new Object({
    make: "Bugatti"
});
myCar.model = "Chiron";
myCar["engine"] = "V16 quad turbo charge";
console.log(myCar);

let carName = myCar["model"];
console.log(carName);

console.log(Object.getOwnPropertyNames(myCar));
console.log(Object.keys(myCar));
console.log(Object.values(myCar));
console.log(myCar.hasOwnProperty("make"));
console.log(myCar.hasOwnProperty("year"));

for (let i in myCar) {
    console.log(i + ": " + myCar[i]);
}

// constructor
function Book(bookName, bookYear, bookAuthor) {
    this.bookName = bookName;
    this.bookYear = bookYear;
    this.bookAuthor = bookAuthor;
}

function Author(authorName, authorAge, authorSex) {
    this.authorName = authorName;
    this.authorAge = authorAge;
    this.authorSex = authorSex;
}

const author = new Author("Stephen Hawking", 76, "m");

let book = new Book("The Theory of Everything", 2006, author);

console.log(book);
console.log("The book's author is " + book.bookAuthor.authorName);

// Object.create
const Car = {
    type: "Hyper Cars",
    getType: function () {
        return this.type
    },
};

const newCar = Object.create(Car);
console.log(newCar.type);
console.log(newCar.getType());

// prototype
Book.prototype.category = null; // assigning a property to Book constructor
book.category = "Science";
console.log(book);

//method
book.getName = function () { // assigning a method to book object
    return this.bookName;
}
console.log(book.getName());

function Person(personName, personAge, personNationality) {
    this.personName = personName;
    this.personAge = personAge;
    this.personNationality = personNationality;
    this.intro = function () {
        console.log(`${this.personName} is ${this.personAge} yO and from ${this.personNationality}.`);
    }
}

const person = new Person("Farhan", 22, "Iran");
console.log(person.personNationality);
person.intro();

// getter and setter method
let calculateSum = {
    a: 3,
    b: 4,
    set c(x) {
        this.a = x;
    },
    get sum() {
        return this.a + this.b;
    }
};
Object.defineProperties(calculateSum, {
    "d": { set: function (x) { this.b = x } }
});

calculateSum.d = 8;
console.log(calculateSum.sum);

console.log(calculateSum.b);
delete calculateSum.b;
console.log(calculateSum.b); // returns undefined

// object reference
myNewCar = myCar;
console.log(myNewCar == myCar);

myCar.model = "Devo";
console.log(myNewCar);

// constructor function
function Employee(name, dept) {
    this.name = name || "";
    this.dept = dept || "general";
};

function WorkerBee(name, dept, reports) {
    // Employee.call(this);
    this.base = Employee;
    this.base(name, dept);
    this.reports = reports || [];
}
WorkerBee.prototype = Object.create(Employee.prototype);
WorkerBee.prototype.constructor = WorkerBee;

let Kim = new WorkerBee("Kim", "Laboratory");
console.log(Kim);

function Engineer(name, reports, machine) {
    // WorkerBee.call(this);
    this.base = WorkerBee;
    this.base(name, "engineering", reports);
    // this.dept = "engineering";
    this.machine = machine || "";
}
Engineer.prototype = new WorkerBee;
Engineer.prototype.constructor = Engineer;
Employee.prototype.specialty = 'whateves'; // adds a property to constructor

let John = new Engineer("John");
John.machine = "agriculture machines";
John.salary = "";
console.log(John);

let Riley = new Engineer;
console.log(Riley); // Riley does not has a salary property
console.log(Riley.specialty);

// Challenge
//The goal of this exercise is to convert a string to a new string 
// where each character in the new string is "(" if that character
// appears only once in the original string, or ")" if that character
// appears more than once in the original string. Ignore capitalization
// when determining if a character is a duplicate.

function duplicateEncoder(str) {
    let strArr = str.toLowerCase().split("");
    let result = "";
    const obj = {};

    for (const key of strArr) {
        if (obj[key]) {
            obj[key] = 2;
        } else {
            obj[key] = 1;
        }
    }
    console.log(obj);

    strArr.forEach(c => {
        if (obj[c] == 1) {
            result += "(";
        } else {
            result += ")";
        }
    });
    return result;
}

console.log(duplicateEncoder("(( @"));

// *** BETTER SOLUTION ***
function duplicateEncoder2(word) {
    return word
        .toLowerCase()
        .split('')
        .map(function (a, i, w) {
            return w.indexOf(a) == w.lastIndexOf(a) ? '(' : ')'
        })
        .join('');
}

console.log(duplicateEncoder2("Farhan"));
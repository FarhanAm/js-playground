// example 1
let target = {};
let proxy2 = new Proxy(target, {});

proxy2.a = "A";
// It is accessible from both target and proxy
console.log(target.a);
console.log(proxy2.a);

// example 2 - get trap
let week = {
    day1: "Saturday",
    day2: "Sunday",
    day3: "Monday"
};

let handler = {
    get: function (week, day) {
        return day in week ? week[day] : "today"
    }
};

let proxy1 = new Proxy (week, handler);

console.log(proxy1.day1);
console.log(proxy1.day5);
proxy1.day5 = "Wednesday";
console.log("proxy:", proxy1.day5);
console.log("target:", week.day5);

// example 3 - get trap
let dict = {
    "Hello": "Hola",
    "Bye": "Adios"
};

dict = new Proxy(dict, {
    get(target, phrase) {
        if (phrase in target) {
            return target[phrase];
        } else {
            return phrase;
        }
    }
});

console.log(dict["Bye"]);
console.log(dict["Welcome to Proxy!"]);

// set trap
let numbers = [];
numbers = new Proxy(numbers, {
    set(target, prop, val) {
        if (typeof val == "number") {
            target[prop] = val;
            return true;
        }
        return false;
    }
});

numbers.push(1);
numbers.push(7);
console.log(numbers);
console.log("length of numbers is:", numbers.length);
// numbers.push("not a number!"); // TypeError
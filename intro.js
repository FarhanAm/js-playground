//---------- Variables and vriable hoisting
console.log(x); // undefined!!!
var x = 12;

//console.log(y); // reference error!!!
let y = 10;

//---------- Function hoisting
sayHi();

function sayHi() {
    console.log("Hi!");
}

// sayHello(); // type error!!!
var sayHello = function() {
    console.log("Hello!");
}
sayHello(); // * works properly!!!

//---------- Consts
// const sayHi = "Hiii"; // syntax error!!!
const obj1 = { key: "value" };
console.log(obj1.key);
obj1.key = "sth else";
console.log(obj1); // * objects assigned to constants are not protected!!!

const arr1 = ["Farhan"];
console.log(arr1);
arr1.push("Farid");
console.log(arr1); // * arrays assigned to constants are not protected!!!

//---------- Data Types and conversions
console.log(23 + " is a prime number!"); // 23 converts to string!!!
console.log("23" + 7); // 7 converts to string!!!
console.log("23" - 7); // 23 converts to number!!!
console.log(parseInt("111", 2)); // the string "111" converts to int 7 with radix 2!!!
console.log("12" + 2);
console.log(+"12" + 2); // converting to int via unary plus!!!

//---------- Literals
let myCars = [, "Sian", "P1", , "Pagani", ,];
console.log(myCars.length); // 6!!!

console.log(0X32); // 50 in hexadecimal number system!!!

let books = {
    1: {
        name: "The theory of everything",
        author: "Stephen Hawking"
    },
    two: {
        name: "Sophie's World",
        author: "Jostein Gaarder"
    }
};
console.log(books[1].name);
console.log(books.two.author);

let userName = "Farhan";
console.log(`Hello "${userName}"`); // * template literal

let escapingChar = 'this string \
is broken \n\
across multiple \
lines.'
console.log(escapingChar);

let objA = {}
let objB = {
    id: "12a21",
    name: "Tony"
}
console.log(objA);
objA.__proto__ = objB; // now we have access to objB properties
console.log(objA.id);
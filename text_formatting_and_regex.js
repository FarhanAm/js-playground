//----------- String formatting
let str = "Mississippi";
console.log(str.indexOf("s"));
console.log(str.lastIndexOf("s"));
console.log(str.indexOf("S")); // it'll return -1 since there isn't S

console.log(str.endsWith("i"));
console.log(str.includes("m"));

let str2 = "State Uni";
str3 = str.concat(" ", str2);
console.log(str3);

let seperateBySs = str.split("ss");
console.log(seperateBySs);

let sliceOfStr = str.slice(2, -4);
console.log(sliceOfStr);

let regex = /[A-Z]/g;
console.log(str.match(regex));

console.log(str2.replace("State", str));

console.log(str.toUpperCase());

console.log("          State   Uni".trim());

//----------- Intl
let today = new Date();
let iranDate = new Intl.DateTimeFormat("fa-IR").format;
console.log(iranDate(today));

// Challenge
// you are required to, given a string, replace every letter with its position in the alphabet
let alphabet = [...Array(26).keys()].map(i => String.fromCharCode(i + 97));

function strToPositionNum(text) {
    let textToLower = text.toLowerCase();
    let result = "";
    for (i in textToLower) {
        if (alphabet.includes(textToLower[i])) {
            result = result + (alphabet.indexOf(textToLower[i]) + 1) + " ";
        }
    }
    if (result[result.length - 1] === " ") {
        result = result.substr(0, result.length - 1);
    }
    return result;
}

console.log(strToPositionNum("The narwhal bacons at midnight."));

// *** BETTER SOLUTION ***
function strToPositionNum2(text) {
    return text
        .toUpperCase()
        .match(/[a-z]/gi)
        .map((c) => c.charCodeAt() - 64)
        .join(' ');
}

console.log(strToPositionNum2("The narwhal bacons at midnight."));
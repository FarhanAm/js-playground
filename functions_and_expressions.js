//---------- Functions
function power (n, m) {
    if (m === 1) {
        return n;
    }
    return n * power(n, m - 1);
}
console.log(power(2, 5));

powerFunc = function pow (n, m) { return m === 1 ? n : n * pow (n, m - 1) };
console.log(powerFunc(2, 6));

// console.log(pwrF(2, 3)); // this will throw a reference error!!!
let pwrF = (n, m) => {
    let result = 1;
    for (let i = 1; i <= m; i++) {
        result *= n;
    }
    return result;
}
console.log(pwrF(2, 10));

// function as an argument!!!
let reverse = (n) => { return parseInt(n.toString().split("").reverse().join("")) };

function map (f, arr) {
    let rslt = [];
    for (let i = 0; i < arr.length; i++) {
        rslt[i] = f(arr[i]);
    }
    return rslt;
}

let myArr = [12, 102, 1234, 54454];
console.log(map(reverse, myArr));
console.log(map.constructor);

function returnMostInnerValue() {
    let x = "Mehran";
    function innerFunc (x) {
        function mostInner (x) {
            return x;
        }
        return mostInner;
    }
    return innerFunc;
}
console.log(returnMostInnerValue ()("Farid")("Farhan")); // interesting!!!!!

let getAccount = function () {
    var accountId = "12ab";
    return function () {
        return accountId;
    }
}
console.log(getAccount()());

// arguments object
function sum () {
    let result = 0;
    for (let i = 0; i < arguments.length; i++) {
        result += arguments[i];
    }
    return result;
}
console.log(sum(1, 33, 23, 10));

// default parameter
function facto (n = 10) {
    if (n === 0) {
        return 1;
    }
    return n * facto (n - 1);
}
console.log(facto());

// rest parameter
function listToPower (power, ...arr) {
    return arr.map(x => Math.pow(x, power));
}
console.log(listToPower(3, 2, 3, 4));


//---------- Expressions
// typeof / in
function isThereAny (theThing, obj) {
    if (typeof(obj) === "object") {
        if (theThing in obj) {
            return "The object includes " + theThing;
        }
        return theThing + " is not in the object";
    }
    throw new Error("second argument must be an object!");
}
let obj1 = { name: "John", surname: "Smith"};
let obj2 = { name: "Jane", surname: "Geller"};
try {
    console.log(isThereAny("name", obj1));
    console.log(isThereAny("author", obj2));
    console.log(isThereAny(3, "hello"));
} catch (e) {
    console.log("Why so serious?!");
}

// map and ternary expression
let myArr1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let mappedArr = myArr1.map(x => x % 2 === 0 ? x : 1);
console.log(mappedArr);
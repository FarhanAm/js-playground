// Default import
import Add from "./add.js";
import Multiply from "./multiply.js";

// named import
import { generateRand } from "./random.js";

let add = new Add(1, 100);
console.log(add.calculate());

let multiply = new Multiply(512, 2);
console.log(multiply.calculte());

console.log(generateRand(10, 15));
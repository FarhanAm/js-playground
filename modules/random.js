function generateRand (min, max) {
    let range = max - min
    let randNum = Math.random() * range;
    let randInRange = Math.floor(randNum + min);
    return randInRange;
}

export { generateRand };
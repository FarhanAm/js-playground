class Add {
    constructor (a, b) {
        this.a = a;
        this.b = b;
    }
    calculate () {
        return this.a + this.b;
    }
}

export default Add;
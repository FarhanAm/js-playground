//------------ Promises
const doWorkPromis = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve({ name: "Stark" });
        reject("Sth went wrong");
    })
});

doWorkPromis.then((result) => {
    console.log("Success!", result);
}).catch((error) => {
    console.log("Error!", error);
});

const add = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(a + b);
        }, 2000);
    });
};

// promise chaining
add(1, 2).then((sum) => {
    console.log("first promise", sum);

    add(sum, 5).then((sum2) => {
        console.log("first promise", sum2);
    }).catch((e) => {
        console.log(e);
    });
}).catch((e) => {
    console.log(e);
});

add(1, 1).then((sum) => {
    console.log("second promise", sum);
    return add(sum, 4);
}).then((sum2) => {
    console.log("second promise", sum2);
    return add(sum2, 5);
}).then((sum3) => {
    console.log("second promise", sum3);
}).catch((e) => {
    console.log(e);
});

//------------ Async Await
const sumFunc = (a, b) => {
    return new Promise ((resolve, reject) => {
        setTimeout(() => {
            if(a < 0 || b < 0) {
                return reject("Numbers must be non-negetive");
            }
            resolve(a + b);
        }, 2000);
    });
};

const sumUp = async () => {
    const sum = await sumFunc(1, 99);
    const sum2 = await sumFunc(sum, 50);
    // const sum3 = await sumFunc(sum2, -3);
    const sum3 = await sumFunc(sum2, 3);
    return sum3;
};

sumUp().then((result) => {
    console.log("Result is", result);
}).catch((e) => {
    console.log(e);
});
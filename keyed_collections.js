//----------- Map

let cars = new Map();
cars.set("Chiron", "V-16");
cars.set("Senna", "V-8");
cars.set("Veneno", "V-12");
console.log(cars);

console.log(cars.get("Veneno"));
console.log(cars.has("Chiron"));

for (let [k, v] of cars) {
    console.log(k + " has a " + v + " engine!");
}

console.log(cars.size + " : " + cars.length); // doesn't has a length property

cars.clear();
console.log(cars.size);

//----------- WeakMap
let wm1 = new WeakMap();
let wm2 = new WeakMap();

let thing1 = {};
let thing2 = function () {}

wm1.set(thing1, "WhatEver");
wm2.set(thing2, 23);

console.log(wm1.get(thing1));
console.log(wm2);

//----------- Sets
let newSet = new Set();
newSet.add("Farhan");
newSet.add(22);
newSet.add("Student");
newSet.add("Developer");
console.log(newSet.add(22));

console.log(newSet.size);

for (let item of newSet) {
    console.log(item);
}

// convertin array to set and vice versa
let myArr = Array.from(newSet);
console.log("Array",myArr);

let myArrBySpreadOp = [...newSet];
console.log("Array by spread", myArrBySpreadOp);

console.log(new Set([1, 2, 4, 7, 7, 1, 2, 3])); // this will remove duplicate items

newSet.delete("Student"); // delete item by its value
console.log(newSet);
var x = 1;
{
    var x = 2;
}
console.log(x);

var y = 2;
{
    let y = 3; // let is block scoped!!!
}
console.log(y);

//----------- If else
let number = 7;
if (number = 7) {
    console.log("Bingo :)");
} else {
    console.log("Maybe next time :(");
}

//----------- Switch Case
let time = "noon";
switch (time) {
    case "morning":
        console.log("eat breakfast");
        break;
    case "noon":
        console.log("eat lunch");
    case "afternoon":
        console.log("drink tea");
        break;
    case "night":
        console.log("eat dinner");
        break;
    default:
        console.log("drink coffe");
}
console.log("end of switch case!");

//----------- Try Catch
function DividedByZero () {
    this.name = "DevidedByZero";
}

DividedByZero.prototype.toString = function () {
    return `${this.name}: "the devisor must not be zero!"`;
}

function errorHandler (num1, num2) {
    return num1 / (num2 + 1);
}

function divide (num1, num2) {
    if (num2 === 0) {
        throw new DividedByZero();
    }
    return num1 / num2;
}
// console.log(divide(10, 0)); // throw an exception with the message: "the devisor must not be zero!"

try {
    console.log(divide(10, 2));
    console.log(divide(10, 0));
} catch (e) {
    console.log(errorHandler(10, 0));
} finally {
    console.log("Anyway! Lets continue.");
}

//*************** CHALLENGE *****************
// make a function that can take any non-negative integer
// as an argument and return it with its digits in descending order

function descendingOrder (n) {
    if (n >= 0) {
        return parseInt(n.toString().split("").sort((a, b) => b - a).join(""));
    } else {
        return "Enter a non-negetive number."
    }
}

console.log(descendingOrder(12343455867995));
//---------- Number
let max_num = Number.MAX_VALUE;
console.log(max_num);

let eps = Number.EPSILON;
console.log(eps);

let min_safe_int = Number.MIN_SAFE_INTEGER;
console.log(min_safe_int);

let neg_infinity = Number.NEGATIVE_INFINITY;
console.log(neg_infinity);

let maybe_octal_num = 023459; // this will give an error but the script will notcrash
console.log(maybe_octal_num); // but its not because of the digit 9

console.log(Number.parseInt(23.4234));

//---------- Math
const pi = Math.PI;
console.log(pi);

let angle = Math.cos(1.0471); // cosine of 60 degree
console.log(angle);

console.log(Math.abs(neg_infinity));

console.log(Math.sqrt(1024));

let rand_num = Math.random();
console.log(rand_num);

// this'll give a random number in range 0 to 10
let rand_num_range10 = Math.floor(rand_num * 10) + 1;
console.log(rand_num_range10);

//----------- Date
let now = new Date();
console.log(now);

let newNow = Date();
console.log(newNow);

let birthday = new Date(1998, 8, 8);
console.log(birthday);

// my age
let age = now.getFullYear() - birthday.getFullYear();
console.log(age);
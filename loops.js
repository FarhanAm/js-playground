//---------- For loops
for (let i = 5; i <= 10; i++) {
    console.log(i * i);
}

//---------- Do While
let i = 5;
do {
    console.log(i);
    i--;
} while (i > 0)

//---------- While
let j = 10;
while (j !== 2) {
    console.log(j - 1);
    j -= 2;
}

//---------- Label and break
let x = 0;
let y = 0;
outer:
    while (x < 2) {
        console.log("outer loop iteration: " + x);
        x++;
        while (y < 5) {
            console.log("inner loop iteration: " + y);
            if (y * 2 == 8) {
                break;
            } else if (y % 10 >= 3) {
                console.log("outer loop reached its end!");
                break outer;
            }
            y++;
        }
    }
    
//---------- Continue
let n = 0;
evenNumbers: while (n <= 5) {
    if (n % 2 === 0) {
        console.log(n + " is an even number!");
    }
    n++;
    continue evenNumbers;
}

//---------- for ... in
let obj = {
    name: "Farhan",
    lName: "Aqaei",
};

for (i in obj) {
    console.log("obj." + i + " = " + obj[i]);
}

//---------- for ... of
let arr = ["Farhan", "Farid", "Farzan"]
for (i of arr) {
    console.log(i); // for accessing values
}

for (i in arr) {
    console.log(i); // for accessing indexes
}